FROM jupyter/scipy-notebook 

#switching to root and installing some packages needed for stablebaselines
USER root
RUN sudo apt-get update && sudo apt-get install -y cmake libopenmpi-dev python3-dev zlib1g-dev git
USER $NB_UID


# for the current versions of those packages in hub.cern.ch
RUN pip install jupyter-client==5.3.4 jupyter-core==4.6.0 jupyterhub==1.0.0 jupyterlab==1.1.4 jupyterlab-server==1.0.6 notebook==6.0.1

# for jupyterlab
run pip install --upgrade jupyterlab-git && \
    jupyter labextension uninstall @bokeh/jupyter_bokeh --no-build && \
    jupyter labextension install @jupyter-widgets/jupyterlab-manager jupyter-matplotlib --no-build && \
    jupyter labextension install jupyterlab_tensorboard --no-build && \
    jupyter lab build && \
    jupyter lab clean && \
    npm cache clean --force && \
    rm -rf /home/$NB_USER/.cache/yarn && \
    rm -rf /home/$NB_USER/.node-gyp && \
    fix-permissions $CONDA_DIR && \
    fix-permissions /home/$NB_USER



# some package(s)
RUN pip install cpymad 


# lines for stablebaselines
RUN pip install stable-baselines[mpi]

    
